import React from 'react';

import classes from './TodoItem.module.css';
import Todo from '../models/Todo';

const TodoItem: React.FC<{item: Todo, onDeleteTodo: () => void}> = props => {
    return (
        <li className={classes['todo-item']}>
            {props.item.text}
            <button onClick={props.onDeleteTodo}>X</button>
        </li>
    );
};

export default TodoItem;