import { useContext, useRef } from 'react';

import classes from './NewTodo.module.css';
import { TodosContext } from '../store/todos-context';

const NewTodo: React.FC = () => {
    const textInputRef = useRef<HTMLInputElement>(null);
    const todosCtx = useContext(TodosContext);
    
    const handleFormSubmit = (event: React.FormEvent) => {
        event.preventDefault();
        const enteredText = textInputRef.current!.value;
        if (enteredText.trim().length === 0) {
            // throw error
            return;
        }
        todosCtx.addTodo(enteredText);
        textInputRef.current!.value = '';
    }

    return (
        <form className={classes.form} onSubmit={handleFormSubmit}>
            <label htmlFor="text">Todo text</label>
            <input
                id="text"
                type="text"
                placeholder="Enter your todo text"
                autoComplete="off"
                ref={textInputRef}
            />
            <button type="submit">Add Todo</button>
        </form>
    );
};

export default NewTodo;