import React, { useState } from 'react';

import Todo from '../models/Todo';

type TodosContextObj = {
    items: Todo[];
    addTodo: (text: string) => void;
    deleteTodo: (id: string) => void;
};

const TodosContext = React.createContext<TodosContextObj>({
    items: [],
    addTodo: () => { },
    deleteTodo: (id: string) => { }
});

const TodosContextProvider: React.FC = (props) => {
    const todosInitialState = [
        new Todo('Learn React'),
        new Todo('Learn Typescript')
    ];
    const [todos, setTodos] = useState<Todo[]>([]);

    const addNewTodoHandler = (todoText: string) => {
        const newTodo = new Todo(todoText);
        setTodos(prevTodos => [...prevTodos, newTodo]);
    };

    const deleteTodoHandler = (todoId: string) => {
        setTodos(prevTodos => prevTodos.filter(todo => todo.id !== todoId));
    };

    const contextValue: TodosContextObj = {
        items: todos,
        addTodo: addNewTodoHandler,
        deleteTodo: deleteTodoHandler
    };
    
    return (
        <TodosContext.Provider
            value={contextValue}
        >
            {props.children}
        </TodosContext.Provider>
    );
}

export {
    TodosContextProvider as default,
    TodosContext
};